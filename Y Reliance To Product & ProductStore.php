<?php
include("../connection.php");

@unlink("Y Reliance_To_P_PS.log");
ini_set("log_errors", 1);
ini_set("error_log", "Y Reliance_To_P_PS.log");

error_log("Reliance To Product & ProductStore Execution Started");

$get_count = 'SELECT max(prdid) FROM `product`';
$res_get_count = mysqli_query($conn, $get_count);
$temp = $res_get_count -> fetch_assoc();
$start_id = $temp["max(prdid)"] + 1;

echo '<b>Started From : '.$start_id.'</b><br/><br/>';

$insId = $start_id;

$get_records = 'SELECT * FROM `Reliance`';
$res_get_records = mysqli_query($conn, $get_records);

//If Record In Reliance--------------------------------------------------------------------------------------------------------
if($res_get_records && $res_get_records -> num_rows > 0) {
	//While Record In Reliance-------------------------------------------------------------------------------------------------
	while($fetch = $res_get_records -> fetch_assoc()) {
		$prdid = $fetch["prdid"];
		$prdname = $fetch["prdname"];
		$prdclr = $fetch["prdclr"];
		$prdcat = $fetch["prdcat"];
		$prdsbcat = $fetch["prdsbcat"];
		$prddesc = $fetch["prddesc"];
		$prdbrnd = $fetch["prdbrnd"];
		$prdimgurl = $fetch["prdimgurl"];
		$prdmrp = $fetch["prdmrp"];
		$sysdate = date("Y/m/d");
		$strid = $fetch["strid"];
		$prdstrmrp = $fetch["prdstrmrp"];
		$prdstrprc = $fetch["prdstrprc"];
		$prdstrfnlprc = $fetch["prdstrfnlprc"];
		$prdstrurl = $fetch["prdstrurl"];
		$prmid = '';
		$prdstravlbl = $fetch["prdstravlbl"];
		
		//error_log("Process Started  For Reliance : prdid => ".$prdid );
		
		$prdname_words = explode(" ", $prdname);
		$c_prdname_words = count($prdname_words);
		
		$temp = "";
		$i = 0;
		while ($i < $c_prdname_words) {
			$temp = $temp ."`prdname` LIKE \"%".$prdname_words[$i]."%\"";
			if (($i + 1) < $c_prdname_words) {	$temp = $temp." AND ";	}
			$i++;
		}
				
		$compare = "SELECT * FROM `product` WHERE (`prdcat` = '$prdcat' AND `prdbrnd` = '$prdbrnd' AND ".$temp.")";
		$res_compare = mysqli_query($conn, $compare);
		echo '<b><u>Comparision Query</u> : '.$compare.'</b><br/>';

		$INSDoneP = 0;
		$INSdonePS = 0;
		$UPdone = 0;	
	//Comparision Query Result---------------------------------------------------------------------------------------------------
		//No Such Product Found--------------------------------------------------------------------------------------------------
		if($res_compare && $res_compare -> num_rows == 0) {
			$insP = "INSERT INTO `product` VALUES ('$insId', '$prdname', '$prdclr', '$prdcat', '$prdsbcat', '$prddesc', '$prdbrnd', '$prdimgurl', '$prdmrp', '$sysdate', '')";
			$insPS = "INSERT INTO `productstore` VALUES ('$insId', '$strid', '$prdstrmrp', '$prdstrprc', '$prdstrurl', '$prmid', '$prdstrfnlprc', '$sysdate', '$prdstravlbl')";
			
			$res_insP = mysqli_query($conn, $insP);
			$res_insPS = mysqli_query($conn, $insPS);
			
			$up_Reliance = "UPDATE `Reliance` SET `insP` = 1, `insPS` = 1, `processed` = 1 WHERE `prdid` = '$prdid'";
			$res_up_Reliance = mysqli_query($conn, $up_Reliance);
			
			echo '<div style="margin-left:100px;"><b><u>Insert Product</u> : </b>'.$insP.'<br/><b><u>Insert ProductStore</u> :</b> '.$insPS.'<br/><b>insP = 1, insPS = 1 & processed = 1</b></div><br/>';
			
			$INSDoneP++;
			$INSdonePS++;
			$insId++;
			error_log("Complete Insertion Done For Reliance => prdid => ".$prdid." At Product => prdid => ".$insId);
		}
		//No Such Product Found--------------------------------------------------------------------------------------------------
		//If Product/s Found-----------------------------------------------------------------------------------------------------
		else if($res_compare && $res_compare -> num_rows > 0) {
			//While Product/s Found----------------------------------------------------------------------------------------------
			while($record = $res_compare -> fetch_assoc()) {
				$this_prdid = $record["prdid"];
				$this_prdname = $record["prdname"];
				
				echo '<div style = "margin-left:100px;"><u>Similar Product : '.$this_prdid.' & Reliance : '.$prdid.'</u>
				<br/>Insert &nbsp;: '.$prdname.'<br/>Found : '.$this_prdname.'</div>';
				
				$this_prdname_words = explode(" ", $this_prdname);
				$c_this_prdname_words = count($this_prdname_words);
				
				$same_words = array_intersect($prdname_words, $this_prdname_words);
				$c_same = count($same_words);
				
				//If Both Names Are Of Same No Of Words--------------------------------------------------------------------------
				//if($c_prdname_words == $c_this_prdname_words && $c_same == $c_prdname_words) {
				if($c_same == $c_this_prdname_words) {
					echo '<div style = "margin-left:100px;"><u><b>Same Product : '.$this_prdid.' & Reliance : '.$prdid.'
																												</b></u></div>';
					
					$get_ps = "SELECT `strid` FROM `productstore` WHERE `prdid` = '$this_prdid'";
					$res_get_ps = mysqli_query($conn, $get_ps);

					//If Records Found In ProductStore---------------------------------------------------------------------------
					if($res_get_ps && $res_get_ps -> num_rows > 0) {
						//While Records Found In ProductStore--------------------------------------------------------------------
						while($ps_rec = $res_get_ps -> fetch_assoc()) {
							$this_strid = $ps_rec["strid"];
							
							//Record With Same Store Id Found In ProductStore----------------------------------------------------
							if($this_strid == $strid) {
								$updatePS = "UPDATE `productstore` SET `prdstrmrp` = '$prdstrmrp', `prdstrprc` = '$prdstrprc', `prdstrurl` = '$prdstrurl', `prmid` = '$prmid', `prdstrfnlprc` = '$prdstrfnlprc', `systemdate` = '$sysdate', `prdstravlbl` = '$prdstravlbl' WHERE `prdid` = '$this_prdid' AND `strid` = '$this_strid'";
								$res_updatePS = mysqli_query($conn, $updatePS);

								$up_Reliance = "UPDATE `Reliance` SET `upPS`=1, `processed`=1 WHERE `prdid`='$prdid'";
								$res_up_Reliance = mysqli_query($conn, $up_Reliance);
								
								$UPdone++;
								echo '<div style="margin-left:100px;"><b><u>PrdId : '.$prdid.' Of Reliance Will Not Be Inserted
								<br/>Update ProductStore</u> :</b> '.$updatePS.'<br/><b>upPS =1 & processed = 1</b></div><br/>';
								
								error_log("Updated PS For Reliance => prdid => ".$prdid." At Product => prdid => ".$this_prdid);
							}
							//Record With Same Store Id Found In ProductStore----------------------------------------------------
						}
						//While Records Found In ProductStore--------------------------------------------------------------------
						if($UPdone == 0) {
							$insPS = "INSERT INTO `productstore` VALUES ('$this_prdid', '$strid', '$prdstrmrp', '$prdstrprc', '$prdstrurl', '$prmid', '$prdstrfnlprc', '$sysdate', '$prdstravlbl')";
							$res_insPS = mysqli_query($conn, $insPS);
							
							$up_Reliance = "UPDATE `Reliance` SET `insPS`=1, `processed`=1 WHERE `prdid`='$prdid'";
							$res_up_Reliance = mysqli_query($conn, $up_Reliance);
							
							echo '<div style="margin-left:100px;"><b><u>PrdId : Reliance => '.$prdid.' Will Not Be Inserted Into
							 Product Table<br/>Insert PS</u> :</b> '.$insPS.'<br/><b>insPS = 1 & processed = 1</b></div><br/>';
							 
							$INSdonePS++;
							error_log("Inserted In PS For Reliance => prdid => ".$prdid." At Product => prdid => ".$this_prdid);
						}
					}
					//If Records Found In ProductStore---------------------------------------------------------------------------
					//No Records Found In ProductStore---------------------------------------------------------------------------
					else {
						$insPS = "INSERT INTO `productstore` VALUES ('$insId', '$strid', '$prdstrmrp', '$prdstrprc', '$prdstrurl', '$prmid', '$prdstrfnlprc', '$sysdate', '$prdstravlbl')";
						$res_insPS = mysqli_query($conn, $insPS);
						
						$up_Reliance = "UPDATE `Reliance` SET `insPS` = 1, `processed` = 1 WHERE `prdid` = '$prdid'";
						$res_up_Reliance = mysqli_query($conn, $up_Reliance);
						
						$INSdonePS++;
						echo '<div style="margin-left:100px;"><b><u>Insert PS</u> :</b> '.$insPS.'<br/>
																					<b>insPS =1 & processed = 1</b></div><br/>';

						error_log("Inserted In PS For Reliance => prdid => ".$prdid." At Product => prdid => ".$this_prdid." There Were No Records In PS For This Product");
					}
					//No Records Found In ProductStore---------------------------------------------------------------------------
				}
				//If Both Names Are Of Same No Of Words--------------------------------------------------------------------------
			}
			//While Product/s Found----------------------------------------------------------------------------------------------
			//If No Updation Or Insertion Done To ProductStore-------------------------------------------------------------------
			if($UPdone == 0 && $INSdonePS == 0) {
				$insP = "INSERT INTO `product` VALUES ('$insId', '$prdname', '$prdclr', '$prdcat', '$prdsbcat', '$prddesc', '$prdbrnd', '$prdimgurl', '$prdmrp', '$sysdate', '')";
				$insPS = "INSERT INTO `productstore` VALUES ('$insId', '$strid', '$prdstrmrp', '$prdstrprc', '$prdstrurl', '$prmid', '$prdstrfnlprc', '$sysdate', '$prdstravlbl')";
				
				$res_insP = mysqli_query($conn, $insP);
				$res_insPS = mysqli_query($conn, $insPS);
				
				$up_Reliance = "UPDATE `Reliance` SET `insP`=1, `insPS`=1, `processed`=1 WHERE `prdid`='$prdid'";
				$res_up_Reliance = mysqli_query($conn, $up_Reliance);
				
				echo '<div style="margin-left:100px;"><b><u>Insert Product</u> : </b>'.$insP.'<br/><b><u>Insert ProductStore</u> :</b> '.$insPS.'<br/><b>insP = 1, insPS = 1 & processed = 1</b></div><br/>';
				
				$INSDoneP++;
				$INSdonePS++;
				$insId++;

				error_log("At Last Complete Insertion Done For Reliance => prdid => ".$prdid." At Product => prdid => ".$insId);
			}
			//If No Updation Or Insertion Done To ProductStore-------------------------------------------------------------------
		}
		//If Product/s Found-----------------------------------------------------------------------------------------------------
		//Some Error Occured-----------------------------------------------------------------------------------------------------
		else {	echo "<center><h1>Something Is Wrong...</h1></center>";	}
		
		if($INSDoneP == 0 && $INSdonePS == 0 && $UPdone == 0) {
			$up_Reliance = "UPDATE `Reliance` SET `err` = 1 WHERE `prdid`='$prdid'";
			$res_up_Reliance = mysqli_query($conn, $up_Reliance);
			echo '<div style="margin-left:100px;"><b>err = 1</b></div><br/>';
		}
		//Some Error Occured-----------------------------------------------------------------------------------------------------
	//Comparision Query Result---------------------------------------------------------------------------------------------------
	}
	//While Record In Reliance-------------------------------------------------------------------------------------------------
}
//If Record In Reliance--------------------------------------------------------------------------------------------------------
else {
	echo '<br/><center>No Records Found In `Reliance` Table To Process</center><br/>';
}

echo '<br/><b>Finished At : '.($insId - 1).'</b><br/>';

error_log("Reliance To Product & ProductStore Execution Finished");

mysqli_close($conn);
?>